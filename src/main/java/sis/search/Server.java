package sis.search;

import java.util.*;
import java.util.concurrent.*;

/**
 * @author Carl Adler(C.A.)
 * */
public class Server extends Thread{
	
	private BlockingQueue<Search> queue = new LinkedBlockingDeque<Search>();
	private ResultsListener listener;
	final static String START_MSG = "started";
	final static String END_MSG = "finished";
	
	private static ThreadLocal<List<String>> threadLog = 
			new ThreadLocal<List<String>>() {
				protected List<String> initialValue() {
					return new ArrayList<String>();
				}
	};
	
	private List<String> completeLog = Collections.synchronizedList(new ArrayList<String>());
	
	public Server(ResultsListener listener) {
		this.listener = listener;
		start();
	}
	
	public void run() {
		while(true) {
			try {
				execute(queue.take());
			} catch (InterruptedException e) {
				break;
			}
		}
	}
	
	public void add(Search search) throws Exception {
		queue.put(search);
	}
	
	private void execute(final Search search) {
		new Thread(new Runnable() {
			public void run() {
				log(START_MSG, search);
				search.execute();
				log(END_MSG, search);
				listener.executed(search);
				completeLog.addAll(threadLog.get());
			}
		}).start();
	}
	
	public void shutDown() {
		this.interrupt();
	}
	
	List<String> getLog() {
		return completeLog;
	}
	
	private void log(String message, Search search) {
		threadLog.get().add(search + " " + message + " at " + new Date());
	}
}
