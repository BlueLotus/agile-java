package sis.search;

/**
 * @author Carl Adler(C.A.)
 * */
public interface ResultsListener {
	public void executed(Search search);
}
