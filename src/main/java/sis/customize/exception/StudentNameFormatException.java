package sis.customize.exception;

/**
 * Customize Exception.
 * @author Carl Adler(C.A.)
 * */
public class StudentNameFormatException extends IllegalArgumentException{

	private static final long serialVersionUID = 1L;

	public StudentNameFormatException(String message){
		super(message);
	}
}
