package sis.customize.exception;

/**
 * @author Carl Adler(C.A.)
 * */
public class SessionException extends Exception{

	private static final long serialVersionUID = 1L;

	public SessionException(Throwable cause){
		super(cause);
	}
}
