package sis.studentinfo;

import java.io.Serializable;
import java.util.*;

/**
 * @author Carl Adler(C.A.)
 * */
public class CourseSession extends Session implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static int count;
	
	private CourseSession(Course course, Date startDate) {
		super(course, startDate);
		CourseSession.incrementCount();
	}
	
	public static Session create(Course course, Date startDate){
		return new CourseSession(course, startDate);
	}
	
	private static void incrementCount(){
		count++;
	}
	
	static void resetCount(){
		count = 0;
	}
	
	static int getCount(){
		return count;
	}

	@Override
	protected int getSessionLength(){
		return 16;
	}

	@Override
	protected int getDaysOfNoCourse() {
		return 4;
	}

}
