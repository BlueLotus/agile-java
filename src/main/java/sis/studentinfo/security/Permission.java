package sis.studentinfo.security;

/**
 * @author Carl Adler(C.A.)
 * */
public enum Permission {
	UPDATE, READ_ONLY;
}
