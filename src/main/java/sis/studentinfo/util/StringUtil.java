package sis.studentinfo.util;

import java.util.List;

/**
 * @author Carl Adler(C.A.)
 * */
public class StringUtil {
	
	public static int occurrences(String string, String substring) {
		int occurrences = 0;
		int length = substring.length();
		final boolean ignoreCase = true;
		for(int i = 0; i < string.length() - substring.length() + 1; i++)
			if(string.regionMatches(ignoreCase, i, substring, 0, length))
				occurrences++;
		return occurrences;
	}
	
	public static String concatenateNumbers(List<? extends Number> list, int decimalplaces) {
		String decimalFormat = "%." + decimalplaces + "f";
		StringBuilder builder = new StringBuilder();
		for(Number element : list) {
			double value = element.doubleValue();
			builder.append(String.format(decimalFormat + "%n", value));
		}
		return builder.toString();
	}
}
