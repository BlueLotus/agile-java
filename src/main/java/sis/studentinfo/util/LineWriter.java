package sis.studentinfo.util;

import java.io.*;

/**
 * @author Carl Adler(C.A.)
 * */
public class LineWriter {
	public static void write(String filename, String[] records) throws IOException {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(filename));
			for(int i = 0; i < records.length ; i++) {
				writer.write(records[i]);
				writer.newLine();
			}
		} finally {
			if(writer != null)
				writer.close();
		}
	}
}
