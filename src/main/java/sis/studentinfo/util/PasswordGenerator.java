package sis.studentinfo.util;

import java.util.Random;

/**
 * @author Carl Adler(C.A.)
 * */
public class PasswordGenerator {
	private String password;
	final private static int PASSWORD_LENGTH = 8;
	private Random random = new Random();
	
	final static char LOW_END_PASSWORD_CHAR = 48;
	final static char HIGH_END_PASSWORD_CHAR = 122;
	
	void setRandom(Random random) {
		this.random = random;
	}
	
	public String generatePassword() {
		StringBuffer buffer = new StringBuffer(PASSWORD_LENGTH);
		for(int i = 0; i < PASSWORD_LENGTH; i++)
			buffer.append(getRandomChar());
		return buffer.toString();
	}
	
	private char getRandomChar(){
		final char max = HIGH_END_PASSWORD_CHAR - LOW_END_PASSWORD_CHAR;
		return (char) (random.nextInt(max) + LOW_END_PASSWORD_CHAR);
	}
	
	public String getPassword() {
		return password;
	}
}
