package sis.studentinfo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import sis.customize.exception.SessionException;

/**
 * @author Carl Adler(C.A.)
 * */
public abstract class Session implements Comparable<Session>, Iterable<Student>, Serializable{

	private static final long serialVersionUID = 1L;
	private Course course;
	private transient List<Student> students = new ArrayList<Student>();
	private Date startDate;
	private int numberOfCredits;
	private URL url;
	
	protected Session(Course course, Date startDate){
		this.course = course;
		this.startDate = startDate;
	}
	
	public Iterator<Student> iterator(){
		return students.iterator();
	}
	
	public String getDepartment() {
		return course.getDepartment();
	}

	public String getNumber() {
		return course.getNumber();
	}

	void setNumberOfCredits(int numberOfCredits) {
		this.numberOfCredits = numberOfCredits;
	}
	
	public int getNumberOfCredits() {
		return numberOfCredits;
	}

	int getNumberOfStudents(){
		return students.size();
	}

	public int compareTo(Session that){
		int compare = this.getDepartment().compareTo(that.getDepartment());
		if(compare != 0)
			return compare;
		return this.getNumber().compareTo(that.getNumber());
	}
	
	public void enroll(Student student){
		student.addCredits(numberOfCredits);
		students.add(student);
	}
	
	Student get(int index){
		return students.get(index);
	}
	
	protected Date getStartDate() {
		return startDate;
	}
	
	public List<Student> getAllStudents(){
		return students;
	}
	
	public URL getUrl() {
		return url;
	}

	public void setUrl(String urlString) throws SessionException {
		try {
			this.url = new URL(urlString);
		} catch (MalformedURLException e) {
			log(e);
			throw new SessionException(e);
		}
	}

	abstract protected int getSessionLength();
	
	abstract protected int getDaysOfNoCourse();
	
	public Date getEndDate(){
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(getStartDate());
		final int daysInWeek = 7;
		int numberOfDays = getSessionLength() * daysInWeek - getDaysOfNoCourse();
		calendar.add(Calendar.DAY_OF_YEAR, numberOfDays);
		return calendar.getTime();
	}
	
	double averageGpaForPartTimeStudents(){
		double total = 0.0;
		int count = 0;
		
		for (Student student : students) {
			if(student.isFullTime())
				continue;
			count++;
			total += student.getGpa();
		}
		if (count == 0) return 0.0;
		return total / count;
	}
	
	private void log(Exception e){
		e.printStackTrace();
	}
	
	private void writeObject(ObjectOutputStream output) throws IOException {
		output.defaultWriteObject();
		output.writeInt(students.size());
		for(Student student : students)
			output.writeObject(student.getLastName());
	}
	
	private void readObject(ObjectInputStream input) throws Exception {
		input.defaultReadObject();
		students = new ArrayList<Student>();
		int size = input.readInt();
		for(int i = 0; i < size; i++) {
			String lastName = (String) input.readObject();
			students.add(Student.findByLastName(lastName));
		}
	}
}

