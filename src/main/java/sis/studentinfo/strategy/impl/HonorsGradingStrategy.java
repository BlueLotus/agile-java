package sis.studentinfo.strategy.impl;

import java.io.Serializable;

import sis.studentinfo.Student.Grade;
import sis.studentinfo.strategy.BasicGradingStrategy;

/**
 * @author Carl Adler(C.A.)
 * */
public class HonorsGradingStrategy extends BasicGradingStrategy implements Serializable{

	private static final long serialVersionUID = 1L;

	public int getGradePointsFor(Grade grade) {
		int points = super.getGradePointsFor(grade);
		if(points > 0)
			points++;
		return points;
	}

}
