package sis.studentinfo.strategy;

import sis.studentinfo.Student;

/**
 * @author Carl Adler(C.A.)
 * */
public interface GradingStrategy {
	int getGradePointsFor(Student.Grade grade);
}
