package sis.studentinfo.strategy;

import java.io.Serializable;

import sis.studentinfo.Student;

/**
 * @author Carl Adler(C.A.)
 * */
public class BasicGradingStrategy implements GradingStrategy, Serializable{

		private static final long serialVersionUID = 1L;
		
		public int getGradePointsFor(Student.Grade grade) {
			return  grade.getPoints();
		}
}
