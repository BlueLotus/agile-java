package sis.studentinfo.summer;

import java.util.*;

import sis.studentinfo.Course;
import sis.studentinfo.Session;

/**
 * @author Carl Adler(C.A.)
 * */
public class SummerCourseSession extends Session{
	
		private static final long serialVersionUID = 1L;

		private SummerCourseSession(Course course, Date startDate){
			super(course, startDate);
		}
		
		public static Session create(Course course, Date startDate){
			return new SummerCourseSession(course, startDate);
		}
		
		@Override
		protected int getSessionLength(){
			return 8;
		}
		
		@Override
		protected int getDaysOfNoCourse() {
			return 2;
		}
		
}
