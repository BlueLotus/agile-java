package sis.report;

import java.io.*;

import sis.studentinfo.*;
import static sis.report.ReportConstant.*;

/**
 * @author Carl Adler(C.A.)
 * */
public class RosterReporter {
	public static final String ROSTER_REPORT_HEADER = "Student%n-%n";
	public static final String ROSTER_REPORT_FOOTER = NEWLINE + "%n# students = %d%n";
	
	private Session session;
	private Writer writer;
	
	public RosterReporter(Session session){
		this.session = session;
	}
	
	void writeReport(String filename) throws IOException{
		Writer bufferedWriter = new BufferedWriter(new FileWriter(filename));
		try {
			writeReport(bufferedWriter);
		} finally{
			bufferedWriter.close();
		}
	}
	
	void writeReport(Writer writer) throws IOException {
		this.writer = writer;
		writeHeader();
		writeBody();
		writeFooter();
	}
	
	public void writeHeader() throws IOException {
		writer.write(String.format(ROSTER_REPORT_HEADER));
	}
	
	public void writeBody() throws IOException {
		for (Student student : session.getAllStudents()) 
			writer.write(String.format(student.getName() + "%n"));
	}
	
	public void writeFooter() throws IOException{
		writer.write(String.format(ROSTER_REPORT_FOOTER, session.getAllStudents().size()));
	}
	
}
