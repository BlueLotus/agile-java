package sis.report;

/**
 * @author Carl Adler(C.A.)
 * */
public class ReportConstant {
	public static final String NEWLINE = System.getProperty("line.separator");
}
