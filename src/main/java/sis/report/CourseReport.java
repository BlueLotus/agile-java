package sis.report;

import java.util.*;

import sis.studentinfo.Session;
import static sis.report.ReportConstant.*;

/**
 * @author Carl Adler(C.A.)
 * */
public class CourseReport {
	private List<Session> sessions = new ArrayList<Session>();
	
	public void add(Session session){
		sessions.add(session);
	}
	
	public String text(){
		Collections.sort(sessions);
		StringBuilder builder = new StringBuilder();
		for (Session session : sessions) {
			builder.append(session.getDepartment() + " " +
									session.getNumber() + NEWLINE);
		}
		return builder.toString();
	}
}
