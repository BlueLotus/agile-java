package sis.clock;

import java.util.Date;

/**
 * @author Carl Adler(C.A.)
 * */
public interface ClockListener {
	public void update(Date date);
}
