package sis.search;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Carl Adler(C.A.)
 * */
@RunWith(Suite.class)
@SuiteClasses({ SearchTest.class, ServerTest.class,
						 SearchSchedulerTest.class})
public class SearchPackageIntegrationTest {

}
