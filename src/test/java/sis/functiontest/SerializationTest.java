package sis.functiontest;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import sis.studentinfo.CourseCatalog;


public class SerializationTest {

	@Test
	public void testLoadToNewVersion() throws ClassNotFoundException, IOException {
		final String filename = "FileKeeper/Catalog/CourseCatalogTest.testAddInObjType.txt";
		CourseCatalog catalog = new CourseCatalog();
		catalog.loadInObjType(filename);
		assertEquals(2, catalog.getSessions().size());
	}

}
