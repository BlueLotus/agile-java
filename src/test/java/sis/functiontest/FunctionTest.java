package sis.functiontest;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

/**
 * @author Carl Adler(C.A.)
 * */
public class FunctionTest {

	@Test
	public void testSortStringsInNewList() {
		List<String> list = new ArrayList<String>();

		list.add("Heller");
		list.add("Kafka");
		list.add("Camus");
		list.add("Boyle");
		
		List<String> sortedList = new ArrayList<String>(list);

		java.util.Collections.sort(sortedList);
		
		assertEquals("Boyle", sortedList.get(0));
		assertEquals("Camus", sortedList.get(1));
		assertEquals("Heller", sortedList.get(2));
		assertEquals("Kafka", sortedList.get(3));
		
		assertEquals("Heller", list.get(0));
		assertEquals("Kafka", list.get(1));
		assertEquals("Camus", list.get(2));
		assertEquals("Boyle", list.get(3));
		
	}
	
	@Test
	public void testStringCompareTo(){
		assertTrue("A".compareTo("B") < 0);
		assertEquals(0, "A".compareTo("A"));
		assertTrue("B".compareTo("A") > 0);
	}
	
	@Test
	public void testOverflow() {
		byte b = Byte.MAX_VALUE;
		assertEquals(Byte.MAX_VALUE + 1, b + 1);
		b += 1;
		assertEquals(Byte.MIN_VALUE, b);
	}
	
	@Test
	public void testCoinFlips() {
		final long seed = 100L;
		final int total = 10;
		
		Random random1 = new Random(seed);
		List<Boolean> flips1 = new ArrayList<Boolean>();
		for(int i = 0; i < total ; i++)
			flips1.add(random1.nextBoolean());
		
		Random random2 = new Random(seed);
		List<Boolean> flips2 = new ArrayList<Boolean>();
		for(int i = 0; i < total; i++)
			flips2.add(random2.nextBoolean());
		
		assertEquals(flips1, flips2);
	}	
}
