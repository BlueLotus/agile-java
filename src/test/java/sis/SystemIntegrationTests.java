package sis;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Carl Adler(C.A.)
 * */
@RunWith(Suite.class)
@SuiteClasses({sis.studentinfo.StudentInfoPackageIntegrationTests.class, 
						sis.report.ReportPackageIntegrationTests.class,
						sis.studentinfo.strategy.StrategyPackageIntegrationTests.class, 
						sis.studentinfo.summer.SummerPackageIntegrationTests.class,
						sis.studentinfo.util.UtilPackageIntegrationTest.class,
						sis.studentinfo.ui.UIPackageIntegrationTest.class,
						sis.db.DBPackageIntegrationTest.class,
						sis.studentinfo.security.SecurityPackageIntegrationTest.class, 
						sis.search.SearchPackageIntegrationTest.class})
public class SystemIntegrationTests {

}
