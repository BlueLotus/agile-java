package sis.studentinfo;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import sis.customize.exception.SessionException;
import static sis.studentinfo.util.DateUtil.createDate;

/**
 * @author Carl Adler(C.A.)
 * */
abstract public class SessionTest {

	private final static int CREDITS = 3;
	private Session session;
	protected Date startDate;
	
	@Before
	public void setUp(){
		startDate = createDate(2013, 1, 6);
		session = createSession(new Course("ENGL", "101"), startDate);
		session.setNumberOfCredits(CREDITS);
	}
	
	abstract protected Session createSession(Course course, Date startDate);
	
	@Test
	public void testCreate() {
		assertEquals("ENGL", session.getDepartment());
		assertEquals("101", session.getNumber());
		assertEquals(0, session.getNumberOfStudents());
		assertEquals(startDate, session.getStartDate());
	}
	
	@Test
	public void testComparable(){
		final Date date = new Date();
		Session sessionA = createSession(new Course("CMSC", "101"), date);
		Session sessionB = createSession(new Course("ENGL", "101"), date);	
		assertTrue(sessionA.compareTo(sessionB) < 0);
		assertTrue(sessionB.compareTo(sessionA) > 0);
		
		Session sessionC = createSession(new Course("CMSC", "101"), date);
		assertEquals(0, sessionA.compareTo(sessionC));
		
		Session sessionD = createSession(new Course("CMSC", "210"), date);
		assertTrue(sessionC.compareTo(sessionD) < 0);
		assertTrue(sessionD.compareTo(sessionC) > 0);
	}
	
	@Test
	public void testEnrollStudents(){		
		Student student1 = new Student("Carl Adler");
		session.enroll(student1);
		assertEquals(CREDITS, student1.getCredits());
		assertEquals(1, session.getNumberOfStudents());
		assertEquals(student1, session.get(0));
		
		Student student2 = new Student("Miffy Adler");
		session.enroll(student2);
		assertEquals(CREDITS, student2.getCredits());
		assertEquals(2, session.getNumberOfStudents());
		assertEquals(student1, session.get(0));
		assertEquals(student2, session.get(1));
	}
	
	@Test
	public void testSessionLength(){
		Session session = createSession(new Course("", ""), new Date());
		assertTrue(session.getSessionLength() > 0);
	}
	
	@Test
	public void testDaysOfNoCourse(){
		Session session = createSession(new Course("", ""), new Date());
		assertTrue(session.getDaysOfNoCourse() > 0);
	}
	
	@Test
	public void testAverageGpaForPartTimeStudents(){
		session.enroll(createFullTimeStudent());
		
		Student partTime1 = new Student("p1");
		partTime1.addGrade(Student.Grade.A);
		session.enroll(partTime1);
		
		session.enroll(createFullTimeStudent());
		
		Student partTime2 = new Student("p2");
		partTime2.addGrade(Student.Grade.B);
		session.enroll(partTime2);
		
		assertEquals(3.5, session.averageGpaForPartTimeStudents(), 0.05);
	}
	
	@Test
	public void testIterate(){
		enrollStudents(session);	
		List<Student> results = new ArrayList<Student>();
		for (Student student : session)
			results.add(student);	
		assertEquals(session.getAllStudents(), results);
	}
	
	@Test
	public void testSessionUrl() throws SessionException {
		final String url = "http://course.langrsoft.com/cmsc300";
		session.setUrl(url);
		assertEquals(url, session.getUrl().toString());
	}
	
	@Test
	public void testInvalidSessionUrl() {
		final String url = "httsp://course.langrsoft.com/cmsc300";
		try {
			session.setUrl(url);
			fail("Expected exception due to invalid protocol in URL");
		} catch (SessionException expectedException) {
			Throwable cause = expectedException.getCause();
			assertEquals(MalformedURLException.class, cause.getClass());
		}
	}
	
	private Student createFullTimeStudent(){
		Student student = new Student("H");
		student.addCredits(Student.CREDITS_REQUIRED_FOR_FULL_TIME);
		return student;
	}
	
	private void enrollStudents(Session session){
		session.enroll(new Student("A"));
		session.enroll(new Student("B"));
		session.enroll(new Student("C"));
	}

}
