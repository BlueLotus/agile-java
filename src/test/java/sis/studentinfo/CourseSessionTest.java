package sis.studentinfo;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

import sis.studentinfo.CourseSession;
import sis.studentinfo.util.DateUtil;
import static sis.studentinfo.util.DateUtil.createDate;

/**
 * @author Carl Adler(C.A.)
 * */
public class CourseSessionTest extends SessionTest{

	@Test
	public void testCourseDates(){		
		Date startDate = DateUtil.createDate(2013, 1, 6);
		Session session = createSession(createCourse(), startDate);
		Date sixteenWeeksOut = createDate(2013, 4, 24);
		assertEquals(sixteenWeeksOut, session.getEndDate());
	}
	
	@Test
	public void testCount(){
		CourseSession.resetCount();
		createSession(createCourse(), new Date());
		assertEquals(1, CourseSession.getCount());
		createSession(createCourse(), new Date());
		assertEquals(2, CourseSession.getCount());
	}
	
	@Override
	protected Session createSession(Course course, Date date){
		return CourseSession.create(course, startDate);
	}
	
	private Course createCourse() {
		return new Course("ENGL", "101");
	}
	
}
