package sis.studentinfo.security;

import static org.junit.Assert.*;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Carl Adler(C.A.)
 * */
public class SecureProxyTest {
	
	final private static String secureMethodName = "secure";
	final private static String insecureMethodName = "insecure";
	private Object object;
	private SecureProxy proxy;
	private boolean secureMethodCalled;
	private boolean insecureMethodCalled;
	
	@Before
	public void setUp() {
		object = new Object() {
			@SuppressWarnings("unused")
			public void secure() {
				secureMethodCalled = true;
			}
			@SuppressWarnings("unused")
			public void insecure() {
				insecureMethodCalled = true;
			}
		};
		proxy = new SecureProxy(object, secureMethodName);
	}
	
	@Test
	public void testSecureMethod() throws Throwable {
		Method secureMethod = 
				object.getClass().getDeclaredMethod(secureMethodName, new Class[] {});
		try {
			proxy.invoke(proxy, secureMethod, new Object[] {});
			fail("Expected PermissionException");
		} catch (PermissionException e) {
			assertFalse(secureMethodCalled);
		}
	}
	
	@Test
	public void testInsecureMethod() throws Throwable {
		Method insecureMethod = 
				object.getClass().getDeclaredMethod(insecureMethodName, new Class[] {});
		proxy.invoke(proxy, insecureMethod, new Object[] {});
		assertTrue(insecureMethodCalled);
	}

}
