package sis.studentinfo.security;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

import org.junit.Before;
import org.junit.Test;

import sis.studentinfo.*;

/**
 * @author Carl Adler(C.A.)
 * */
public class AccountFactoryTest {
	
	private List<Method> updateMethods;
	private List<Method> readOnlyMethods;
	
	@Before
	public void setUp() throws Exception{
		updateMethods = new ArrayList<Method>();
		addUpdateMethod("setBankAba", String.class);
		addUpdateMethod("setBankAccountNumber", String.class);
		addUpdateMethod("setBankAccountType", Account.BankAccountType.class);
		addUpdateMethod("transferFromBank", BigDecimal.class);
		addUpdateMethod("credit", BigDecimal.class);
		
		readOnlyMethods = new ArrayList<Method>();
		addReadOnlyMethod("getBalance");
		addReadOnlyMethod("transactionAverage");
	}

	@Test
	public void testUpdateAccess() throws Exception {
		Accountable account = AccountFactory.create(Permission.UPDATE);
		for(Method method : readOnlyMethods)
			verifyNoException(method, account);
		for(Method method : updateMethods)
			verifyNoException(method, account);
	}
	
	@Test
	public void testReadOnlyAccess() throws Exception {
		Accountable account = AccountFactory.create(Permission.READ_ONLY);
		for(Method method : updateMethods)
			verifyException(PermissionException.class, method, account);
		for(Method method : readOnlyMethods)
			verifyNoException(method, account);
	}
	
	private void addUpdateMethod(String name, Class<?> paramClass) throws Exception {
		updateMethods.add(Accountable.class.getDeclaredMethod(name, paramClass));
	}
	
	private void addReadOnlyMethod(String name) throws Exception {
		Class<?>[] noParms = new Class[] {};
		readOnlyMethods.add(Accountable.class.getDeclaredMethod(name, noParms));
	}
	
	private void verifyException(Class<PermissionException> exceptionType, Method method, Object object) throws Exception {
		try {
			method.invoke(object, nullParmsFor(method));
			fail("Expected exception");
		} catch (InvocationTargetException e) {
			assertEquals("Expected exception", exceptionType, e.getCause().getClass());
		}
	}
	
	private void verifyNoException(Method method, Object object) throws Exception {
		try {
			method.invoke(object, nullParmsFor(method));
		} catch (InvocationTargetException e) {
			assertFalse("Unexpected permission exception", PermissionException.class == e.getCause().getClass());
		}
	}
	
	private Object[] nullParmsFor(Method method) {
		return new Object[method.getParameterTypes().length];
	}

}
