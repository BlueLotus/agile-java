package sis.studentinfo.security;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Carl Adler(C.A.)
 * */
@RunWith(Suite.class)
@SuiteClasses({ AccountFactoryTest.class, SecureProxyTest.class })
public class SecurityPackageIntegrationTest {

}
