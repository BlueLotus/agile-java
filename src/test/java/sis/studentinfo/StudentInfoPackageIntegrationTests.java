package sis.studentinfo;

import org.junit.runner.*;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Carl Adler(C.A.)
 * */
@RunWith(Suite.class)
@SuiteClasses({ CourseSessionTest.class, StudentTest.class , 
						 PerformanceTest.class, ScorerTest.class, 
						 StudentDirectoryTest.class, CourseTest.class, 
						 AccountTest.class, CourseCatalogTest.class,
						 MultithreadedAccountTest.class})
public class StudentInfoPackageIntegrationTests {
	
}
