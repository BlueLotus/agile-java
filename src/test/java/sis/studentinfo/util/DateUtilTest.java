package sis.studentinfo.util;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

import sis.studentinfo.util.DateUtil;

/**
 * @author Carl Adler(C.A.)
 * */
public class DateUtilTest {

	@Test
	public void testCreateDate() {
		Date date = DateUtil.createDate(2000, 1 ,1);
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		assertEquals(2000, calendar.get(Calendar.YEAR));
		assertEquals(Calendar.JANUARY, calendar.get(Calendar.MONTH));
		assertEquals(1, calendar.get(Calendar.DAY_OF_MONTH));
	}

}
