package sis.studentinfo.util;

import java.io.File;

import org.junit.Assert;

/**
 * @author Carl Adler(C.A.)
 * */
public class TestUtil {
	
	public static void assertGone(String... filenames) {
		for(String filename : filenames)
			Assert.assertFalse(new File(filename).exists());
	}
	
	public static void delete(String filename) {
		File file = new File(filename);
		if(file.exists())
			Assert.assertTrue(file.delete());
	} 
}
