package sis.studentinfo.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Carl Adler(C.A.)
 * */
@RunWith(Suite.class)
@SuiteClasses({ PasswordGeneratorTest.class, DateUtilTest.class,
						 IOUtilTest.class, StringUtilTest.class,
						 LineWritetTest.class, MultiHashMapTest.class})
public class UtilPackageIntegrationTest {

}
