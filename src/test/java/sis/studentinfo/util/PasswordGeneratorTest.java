package sis.studentinfo.util;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

/**
 * @author Carl Adler(C.A.)
 * */
public class PasswordGeneratorTest {

	@Test
	public void testGeneratePassword() {
		PasswordGenerator generator = new PasswordGenerator();
		generator.setRandom(new MockRandom('A'));
		assertEquals("ABCDEFGH", generator.generatePassword());
		generator.setRandom(new MockRandom('C'));
		assertEquals("CDEFGHIJ", generator.generatePassword());
	}
	
	class MockRandom extends Random {
		private static final long serialVersionUID = 1L;
		private int i;
		public MockRandom(char startCharValue) {
			i = startCharValue - PasswordGenerator.LOW_END_PASSWORD_CHAR;
		}
		protected int next(int bits){
			return i++;
		}
	}

}
