package sis.studentinfo.util;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Before;
import org.junit.Test;


/**
 * @author Carl Adler(C.A.)
 * */
public class MultiHashMapTest {

	final private static Date today = new Date();
	final private static Date tomorrow = new Date(today.getTime() + 86400000);
	final private static String eventA = "wake up";
	final private static String eventB = "eat";
	
	private MultiHashMap<Date, String> events;
	
	@Before
	public void setUp() {
		events = new MultiHashMap<Date, String>();
	}
	
	@Test
	public void testCreate() {
		assertEquals(0, events.size());
	}
	
	@Test
	public void testSingleEntry() {
		events.put(today, eventA);
		assertEquals(1, events.size());
		assertEquals(eventA, getSoleEvent(today));		
	}
	
	@Test
	public void testMultipleEntriesDifferentKey() {
		events.put(today, eventA);
		events.put(tomorrow, eventB);
		assertEquals(2, events.size());
		assertEquals(eventA, getSoleEvent(today));
		assertEquals(eventB, getSoleEvent(tomorrow));
	}
	
	@Test
	public void testMultipleEntriesSameKey() {
		events.put(today, eventA);
		events.put(today, eventB);
		assertEquals(1, events.size());
		Collection<String> retrievedEvents = events.get(today);
		assertEquals(2, retrievedEvents.size());
		assertTrue(retrievedEvents.contains(eventA));
		assertTrue(retrievedEvents.contains(eventB));
	}
	
	@Test
	public void testFilter() {
		MultiHashMap<String, java.sql.Date> meetings = new MultiHashMap<String, java.sql.Date>();
		meetings.put("iteration start", createSqlDate(2013, 10, 7));
	    meetings.put("iteration start", createSqlDate(2013, 10, 14));
	    meetings.put("VP blather", createSqlDate(2013, 10, 7));
	    meetings.put("brown bags", createSqlDate(2013, 10, 9));
		
		MultiHashMap<String, Date> mondayMeetings = new MultiHashMap<String, Date>();
		MultiHashMap.filter(mondayMeetings, meetings, 
				new MultiHashMap.Filter<Date>() {
					public boolean apply(Date date) {
						return isMonday(date);
					}
		});
		
		assertEquals(2, mondayMeetings.size());
		assertEquals(2, mondayMeetings.get("iteration start").size());
		assertNull(mondayMeetings.get("brown bags"));
		assertEquals(1, mondayMeetings.get("VP blather").size());
	}
	
	private String getSoleEvent(Date date) {
		Collection<String> retrievedEvents = events.get(date);
		assertEquals(1, retrievedEvents.size());
		Iterator<String> it = retrievedEvents.iterator();
		return it.next();
	}
	
	private boolean isMonday(Date date) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
	}
	
	private java.sql.Date createSqlDate(int year, int month, int day) {
		Date date = DateUtil.createDate(year, month, day);
		return new java.sql.Date(date.getTime());
	}

}
