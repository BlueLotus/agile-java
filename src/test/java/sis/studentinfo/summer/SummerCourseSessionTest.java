package sis.studentinfo.summer;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import sis.studentinfo.*;
import sis.studentinfo.util.DateUtil;

public class SummerCourseSessionTest extends SessionTest{

	@Test
	public void testEndDate() {
		Date startDate = DateUtil.createDate(2013, 6, 9);
		Session session = createSession(new Course("ENGL", "200"), startDate);
		Date eightWeeksOut = DateUtil.createDate(2013, 8, 2);
		assertEquals(eightWeeksOut, session.getEndDate());
	}
	
	@Override
	protected Session createSession(Course course, Date startDate) {
		return SummerCourseSession.create(course, startDate);
	}

}
