package sis.studentinfo.summer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SummerCourseSessionTest.class })
public class SummerPackageIntegrationTests {

}
