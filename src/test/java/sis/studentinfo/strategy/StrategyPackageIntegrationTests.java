package sis.studentinfo.strategy;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Carl Adler(C.A.)
 * */
@RunWith(Suite.class)
@SuiteClasses({ BasicGradingStrategyTest.class, HonorsGradingStrategyTest.class })
public class StrategyPackageIntegrationTests {

}
