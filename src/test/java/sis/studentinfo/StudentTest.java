package sis.studentinfo;

import static org.junit.Assert.*;

import java.util.logging.Handler;

import org.junit.Test;

import sis.customize.exception.StudentNameFormatException;
import sis.studentinfo.Student;
import sis.studentinfo.strategy.impl.HonorsGradingStrategy;

/**
 * @author Carl Adler(C.A.)
 * */
public class StudentTest {
	
	private final static double GRADE_TOLERANCE = 0.05;

	@Test
	public void testCreate() {
		final String firstStudentName = "Carl Adler";
		final String secondStudentName = "Quepaso";
		final String thirdStudentName = "Miffy D Adler";
		
		final Student student= new Student(firstStudentName);
		assertEquals(firstStudentName, student.getName());
		assertEquals("Carl", student.getFirstName());
		assertEquals("Adler", student.getLastName());
		assertEquals("", student.getMiddleName());
		
		final Student student2 = new Student(secondStudentName);
		assertEquals(secondStudentName, student2.getName());
		assertEquals(" ", student2.getFirstName());
		assertEquals("Quepaso", student2.getLastName());
		assertEquals("", student2.getMiddleName());
		
		final Student student3 = new Student(thirdStudentName);
		assertEquals(thirdStudentName, student3.getName());
		assertEquals("Miffy", student3.getFirstName());
		assertEquals("D", student3.getMiddleName());
		assertEquals("Adler", student3.getLastName());
	}
	
	@Test
	public void testStudentStatus(){
		Student student = new Student("Credit Guy");
		assertEquals(0, student.getCredits());
		assertFalse(student.isFullTime());
		
		student.addCredits(3);
		assertEquals(3, student.getCredits());
		assertFalse(student.isFullTime());
		
		student.addCredits(4);
		assertEquals(7, student.getCredits());
		assertFalse(student.isFullTime());
		
		student.addCredits(5);
		assertEquals(Student.CREDITS_REQUIRED_FOR_FULL_TIME, student.getCredits());
		assertTrue(student.isFullTime());
	}
	
	@Test
	public void testInState(){
		Student student = new Student("Obama");
		assertFalse(student.isInState());
		student.setState(Student.IN_STATE);
		assertTrue(student.isInState());
		student.setState("MD");
		assertFalse(student.isInState());
	}
	
	@Test
	public void testCalculateGpa(){
		Student student = new Student("Carl");
		assertGpa(student, 0.0);
		student.addGrade(Student.Grade.A);
		assertGpa(student, 4.0);
		student.addGrade(Student.Grade.B);
		assertGpa(student, 3.5);
		student.addGrade(Student.Grade.C);
		assertGpa(student, 3.0);
		student.addGrade(Student.Grade.D);
		assertGpa(student, 2.5);
		student.addGrade(Student.Grade.F);
		assertGpa(student, 2.0);
	}
	
	@Test
	public void testCalculateHonorsStudentGpa(){
		assertGpa(createHonorsStudent(), 0.0);
		assertGpa(createHonorsStudent(Student.Grade.A), 5.0);
		assertGpa(createHonorsStudent(Student.Grade.B), 4.0);
		assertGpa(createHonorsStudent(Student.Grade.C), 3.0);
		assertGpa(createHonorsStudent(Student.Grade.D), 2.0);
		assertGpa(createHonorsStudent(Student.Grade.F), 0.0);
	}
	
	@Test
	public void testCharges(){
		Student student = new Student("ROCK");
		student.addCharge(500);
		student.addCharge(200);
		student.addCharge(399);
		assertEquals(1099, student.totalCharges());
	}
	
	@Test
	public void testBadlyFormattedName() {
		Handler handler = new TestHandler();
		Student.logger.addHandler(handler);
		
		final String studentName = "a b c d";
		try {
			new Student(studentName);
			fail("expected exception from 4-part name");
		} catch (StudentNameFormatException expectedException) {
			String message = String.format("Student name '%s' contains more than %d parts", 
					studentName, Student.MAX_NAME_PARTS);
			assertEquals(message, expectedException.getMessage());
			assertEquals(message, ((TestHandler)handler).getMessage());
		}
	}
	
	@Test
	public void testFlags() {
		Student student = new Student("Miffy Adler");
		student.set(Student.Flag.ON_CAMPUS,
						  Student.Flag.TAX_EXEMPT,
						  Student.Flag.MINOR);
		assertTrue(student.isOn(Student.Flag.ON_CAMPUS));
		assertTrue(student.isOn(Student.Flag.TAX_EXEMPT));
		assertTrue(student.isOn(Student.Flag.MINOR));
		
		assertFalse(student.isOff(Student.Flag.ON_CAMPUS));
		assertTrue(student.isOff(Student.Flag.TROUBLEMAKER));
		
		student.unset(Student.Flag.ON_CAMPUS);
		assertTrue(student.isOff(Student.Flag.ON_CAMPUS));
		assertTrue(student.isOn(Student.Flag.TAX_EXEMPT));
		assertTrue(student.isOn(Student.Flag.MINOR));
	}
	
	private void assertGpa(Student student, double expectedGpa){
		assertEquals(expectedGpa, student.getGpa(), GRADE_TOLERANCE);
	}
	
	private Student createHonorsStudent(Student.Grade grade){
		Student student = createHonorsStudent();
		student.addGrade(grade);
		return student;
	}
	
	private Student createHonorsStudent(){
		Student student = new Student("Carl Honor");
		student.setGradingStrategy(new HonorsGradingStrategy());
		return student;
	}

}
