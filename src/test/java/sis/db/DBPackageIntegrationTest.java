package sis.db;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DataFileTest.class, KeyFileTest.class })
public class DBPackageIntegrationTest {

}
