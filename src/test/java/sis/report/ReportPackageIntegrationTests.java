package sis.report;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author Carl Adler(C.A.)
 * */
@RunWith(Suite.class)
@SuiteClasses({ RosterReportTest.class, CourseReportTest.class, 
						 ReportCardTest.class})
public class ReportPackageIntegrationTests {

}
